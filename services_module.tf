module "service" {
  //Using a single 'generic' folder to create parametrized unique services that will each have their own task.
  source = "./services"
  //With this for_each sintax the module will be created one time for each object on the services_list.
  for_each = local.services_list
  //With this each.value sintax I'll grab the attributes defined within the objects inside my list above.
  app_name                = each.value.app_name
  release_version         = each.value.release_version
  environment             = each.value.environment
  aws_account             = each.value.aws_account
  aws_region              = each.value.aws_region
  tfd_cluster_object      = each.value.tfd_cluster_object
  tfd_service_role_object = each.value.tfd_service_role_object
  tfd_subnet_object       = each.value.tfd_subnet_object
}
