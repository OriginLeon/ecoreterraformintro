locals {
  // Creating a list of definition objects to be used on the dynamic modules later.
  services_list = {

    // Using the merge function, witch will join the left side "map-like" object with the right side one.
    // The right side takes precedence so it's ideal for overriding only desired values while still using the defaults.
    example-backend = merge(local.services_default_properties, {
      app_name        = "example-backend"
      release_version = "latest"
    })
  }

}
