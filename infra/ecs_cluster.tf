// Declaration of ecs cluster for the selected environment
resource "aws_ecs_cluster" "tfd_ecs_cluster" {
  name = "tfd-${var.environment}"
}

// Declaration of scaling configuration for the above cluster
resource "aws_autoscaling_group" "tfd_ecs_cluster" {
  name                      = "ECS-${aws_ecs_cluster.tfd_ecs_cluster.name}"
  min_size                  = var.autoscale_min
  max_size                  = var.autoscale_max
  desired_capacity          = var.autoscale_desired
  health_check_grace_period = 300
  health_check_type         = "EC2"
  launch_configuration      = aws_launch_configuration.tfd_launch_configuration.name
  vpc_zone_identifier       = [aws_subnet.tfd_subnet.id]

  lifecycle {
    create_before_destroy = true
  }
}

// This data source will search among available docker images on AWS to host our stuff (according to provided filters)
data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name = "name"
    values = [
      "amzn2-ami-ecs-gpu-hvm-*"
    ]
  }

  filter {
    name = "owner-alias"
    values = [
      "amazon"
    ]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

// Joining together selected docker images, along with the configured instance size from the variables,
// pre-configured security_group and instance_profile (both on security.tf file)
resource "aws_launch_configuration" "tfd_launch_configuration" {
  name                        = "ECS-${aws_ecs_cluster.tfd_ecs_cluster.name}"
  image_id                    = data.aws_ami.amazon_linux.id
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.tfd_ecs_security_group.id]
  iam_instance_profile        = aws_iam_instance_profile.tfd_iam_ecs_profile.name
  enable_monitoring           = true
  associate_public_ip_address = true
  // IMPORTANT!!!
  // The command line below will define the ECS_CLUSTER environment variable inside the ecs config folder.
  // This is done so my services are initialized within the created cluster instead of the default one.
  user_data = <<EOF
    #!/bin/bash
    echo ECS_CLUSTER=tfd-${var.environment} >> /etc/ecs/ecs.config
  EOF

  root_block_device {
    volume_type           = "standard"
    volume_size           = 35
    delete_on_termination = true
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [image_id]
  }
}

// This is like an export. Whatever is outputted will be available outside of this module,
// and can be passed down to other modules if needed.
output "tfd_cluster_object" {
  value = aws_ecs_cluster.tfd_ecs_cluster
}
