// On this file we have our VPC example configuration, along with it's other related features

resource "aws_vpc" "tfd_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    "Name"                          = "tfd-${var.environment}-vpc"
    "aws:cloudformation:stack-id"   = "arn:aws:cloudformation:${var.aws_region}:${var.aws_account}:stack/EC2ContainerService-${var.environment}"
    "aws:cloudformation:logical-id" = "Vpc"
    "aws:cloudformation:stack-name" = "EC2ContainerService-${var.environment}"
  }

  lifecycle {
    ignore_changes = [tags]
  }
}

// Configuration of the internet_gateway
resource "aws_internet_gateway" "tfd_internet_gateway" {
  vpc_id = aws_vpc.tfd_vpc.id
}

resource "aws_route_table" "external" {
  vpc_id = aws_vpc.tfd_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tfd_internet_gateway.id
  }
}

resource "aws_route_table_association" "external-main" {
  subnet_id      = aws_subnet.tfd_subnet.id
  route_table_id = aws_route_table.external.id
}


resource "aws_subnet" "tfd_subnet" {
  vpc_id                  = aws_vpc.tfd_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "${var.aws_region}a"
  map_public_ip_on_launch = true
}


resource "aws_security_group" "tfd_load_balancer_security_group" {
  name        = "tfd_lb_${var.environment}_security_group"
  description = "Allows all traffic"
  vpc_id      = aws_vpc.tfd_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "tfd_ecs_security_group" {
  name        = "tfd_ecs_${var.environment}_security_group"
  description = "Allows all traffic"
  vpc_id      = aws_vpc.tfd_vpc.id

  # individual machines.
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.tfd_load_balancer_security_group.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// This is like an export. Whatever is outputted will be available outside of this module,
// and can be passed down to other modules if needed.
output "tfd_subnet_object" {
  value = aws_subnet.tfd_subnet
}
