// On this file we have the declaration of different roles and policies we'll use on our resources.
// The actual JSON configuration used here are separately defined on another folder to improve maintainability.

resource "aws_iam_role" "tfd_ecs_host_role" {
  name               = "tfd_${var.environment}_ecs_host_role"
  assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "tfd_ecs_instance_role_policy" {
  name   = "tfd_${var.environment}_ecs_instance_role_policy"
  policy = file("policies/ecs-instance-role-policy.json")
  role   = aws_iam_role.tfd_ecs_host_role.id
}

resource "aws_iam_role" "tfd_ecs_service_role" {
  name               = "tfd_${var.environment}_ecs_service_role"
  assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "tfd_ecs_service_role_policy" {
  name   = "tfd_${var.environment}_ecs_service_role_policy"
  policy = file("policies/ecs-service-role-policy.json")
  role   = aws_iam_role.tfd_ecs_service_role.id
}

resource "aws_iam_instance_profile" "tfd_iam_ecs_profile" {
  name = "tfd_${var.environment}_iam_ecs_profile"
  role = aws_iam_role.tfd_ecs_host_role.name
}

// This is like an export. Whatever is outputted will be available outside of this module,
// and can be passed down to other modules if needed.
output "tfd_service_role_object" {
  value = aws_iam_role.tfd_ecs_service_role
}
