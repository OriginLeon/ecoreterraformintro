
variable "environment" {}

variable "aws_region" {}

variable "aws_account" {}

variable "autoscale_min" {
  description = "Minimum autoscale (number of EC2)"
}

variable "autoscale_max" {
  description = "Maximum autoscale (number of EC2)"
}

variable "autoscale_desired" {
  description = "Desired autoscale (number of EC2)"
}

variable "instance_type" {}

