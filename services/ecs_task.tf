resource "aws_ecs_task_definition" "tfd_ecs_task_definition" {
  family = "tfd-${var.environment}-${var.app_name}"
  container_definitions = jsonencode([
    {
      name      = "tfd-${var.environment}-${var.app_name}"
      image     = "${var.aws_account}.dkr.ecr.${var.aws_region}.amazonaws.com/tfd-${var.environment}-${var.app_name}:${var.release_version}"
      cpu       = 0
      memory    = 2048
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 8080
          protocol      = "tcp"
        }
      ]
    }
  ])
}
