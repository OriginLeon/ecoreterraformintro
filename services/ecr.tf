resource "aws_ecr_repository" "tfd_ecr_repository" {
  name                 = "tfd-${var.environment}-${var.app_name}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  encryption_configuration {
    encryption_type = "AES256"
  }
}

resource "aws_ecr_repository_policy" "tfd_ecr_repository_policy" {
  repository = aws_ecr_repository.tfd_ecr_repository.name
  policy     = file("policies/ecr-repository-policy.json")
}
