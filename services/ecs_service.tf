data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_ecs_service" "tfd_service" {
  name            = "tfd-${var.environment}-${var.app_name}"
  cluster         = var.tfd_cluster_object.id
  task_definition = aws_ecs_task_definition.tfd_ecs_task_definition.arn
  desired_count   = 1
  iam_role        = var.tfd_service_role_object.arn

  load_balancer {
    elb_name       = aws_elb.tfd_elb.name
    container_name = "tfd-${var.environment}-${var.app_name}"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_elb" "tfd_elb" {
  name    = "tfd-${var.environment}-${var.app_name}"
  subnets = [var.tfd_subnet_object.id]

  listener {
    lb_protocol = "http"
    lb_port     = 80

    instance_protocol = "http"
    instance_port     = 8080
  }

  cross_zone_load_balancing = true
}


