
variable "app_name" {}

variable "environment" {}

variable "aws_account" {}

variable "aws_region" {}

variable "release_version" {
  default = "latest"
}

variable "tfd_cluster_object" {
  type = object(
    {
      id   = string
      name = string
  })
}

variable "tfd_service_role_object" {
  type = object(
    {
      id   = string
      name = string
      arn  = string
  })
}

variable "tfd_subnet_object" {
  type = object(
    {
      id  = string
      arn = string
  })
}
