// Module to declare the common part of the infrastructure, wich needs to be created only once each environment
module "tfd_cluster" {
  source            = "./infra"
  environment       = var.environment
  aws_region        = var.aws_region
  aws_account       = var.aws_account
  autoscale_min     = lookup(local.infra_environment_attributes_map.autoscale_min, var.environment)
  autoscale_max     = lookup(local.infra_environment_attributes_map.autoscale_max, var.environment)
  autoscale_desired = lookup(local.infra_environment_attributes_map.autoscale_desired, var.environment)
  instance_type     = lookup(local.infra_environment_attributes_map.instance_type, var.environment)
}

