locals {
  services_default_properties = {
    environment             = var.environment
    aws_account             = var.aws_account
    aws_region              = var.aws_region
    tfd_cluster_object      = module.tfd_cluster.tfd_cluster_object
    tfd_service_role_object = module.tfd_cluster.tfd_service_role_object
    tfd_subnet_object       = module.tfd_cluster.tfd_subnet_object
  }
}
