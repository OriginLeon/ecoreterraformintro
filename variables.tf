variable "aws_access_key_id" {
  type        = string
  description = "The Access Key Id for the terraform user for the primary AWS account"
}

variable "aws_secret_access_key" {
  type        = string
  description = "The Secret Access Key for the terraform user for the primary AWS account"
}

variable "aws_region" {
  type        = string
  description = "The default region for the AWS provider"
  default     = "sa-east-1" //BR
}

variable "aws_account" {
  type        = string
  description = "The AWS account number"
}

variable "environment" {
  type        = string
  description = "The name of the environment that is currently being deployed to, i.e. staging or production"
  default     = "develop"
}

