Separate inner terraform project that manages the remote repository
to be used on the actual project.

This should be initialized from inside it's folder separately with "Terraform init".

Currently I'm importing a module that creates the s3 bucket and a state lock within DynamoDB.
The state file of this remote should be commited as well.
