// Remote backend bucket to store the main project STATE
module "tf_backend" {
  source        = "git::https://github.com/DNXLabs/terraform-aws-backend?ref=1.2.1"
  bucket_prefix = "ecore-example-intro"
  assume_policy = {
    all = "arn:aws:iam::240861462251:root"
  }
  workspaces = []
}


