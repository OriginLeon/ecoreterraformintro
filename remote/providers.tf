terraform {
  required_version = ">= 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  access_key = var.remote_aws_access_key_id
  secret_key = var.remote_aws_secret_access_key
  region     = var.remote_aws_region
}
