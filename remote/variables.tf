variable "remote_aws_access_key_id" {
  type        = string
  description = "The Access Key Id for the terraform user for the primary AWS account"
}

variable "remote_aws_secret_access_key" {
  type        = string
  description = "The Secret Access Key for the terraform user for the primary AWS account"
}

variable "remote_aws_region" {
  type        = string
  description = "The default region for the AWS provider"
  default     = "sa-east-1" //BR - Não alterar!
}

