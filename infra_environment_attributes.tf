// Here we have some declared local environments.
// With these i can have different scaling policies values between staging and production for example

locals {

  infra_environment_attributes_map = {

    "autoscale_min" : {
      "develop" : "1",
      "staging" : "1",
      "prod" : "2"
    },
    "autoscale_max" : {
      "develop" : "2",
      "staging" : "2",
      "prod" : "5"
    },
    "autoscale_desired" : {
      "develop" : "2",
      "staging" : "2",
      "prod" : "3"
    },
    "instance_type" : {
      "develop" : "t2.small",
      "staging" : "t2.small",
      "prod" : "t2.large"
    }

  }

}
